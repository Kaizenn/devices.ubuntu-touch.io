---
name: "Oneplus 5/5T"
deviceType: "phone"
image: "https://wiki.lineageos.org/images/devices/cheeseburger.png"
maturity: .6
seo:
  description: 'Switch your Oneplus 5 or 5T to Ubuntu Touch, as your open source daily driver OS.'
  keywords: 'Oneplus 5, Oneplus 5T, Linux on Mobile'


contributors:
  - name: 'Vince1171'
    photo: ''
    forum: '#'
  - name: 'Jami'
    photo: ''
    forum: '#'
  - name: 'Flohack'
    photo: ''
    forum: '#'

externalLinks:
  - name: 'Forum Post'
    link: 'https://forums.ubports.com/category/53/oneplus-5-5t'
    icon: 'yumi'
  - name: 'Repository: Oneplus 5'
    link: 'https://github.com/Flohack74/android_device_oneplus_cheeseburger'
    icon: 'github'
  - name: 'Repository: Oneplus 5T'
    link: 'https://github.com/Flohack74/android_device_oneplus_dumpling'
    icon: 'github'
---
