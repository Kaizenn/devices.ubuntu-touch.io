---
name: 'Sony Xperia X Performance (F8131 & F8132)'
deviceType: 'phone'
maturity: .8

externalLinks:
  -
    name: 'Forum Post'
    link: 'https://forums.ubports.com/topic/4147/sony-xperia-x-performance-dora-f8131-f8132'
    icon: 'yumi'
---
