---
name: 'Meizu MX4'
deviceType: 'phone'
maturity: .9
seo:
  description: 'Switch your Meizu MX4 smartphone OS to Ubuntu Touch, a privacy focus operating system, developed by hundreds of volunteers around the world.'
  keywords: 'Meizu MX4, linux for smartphone, Linux on Phone'
---

Meizu MX 4 devices that are sold with Android have a locked bootloader, so those need to be [manually unlocked by installing the manufacturers Ubuntu image](https://docs.ubports.com/en/latest/userguide/install.html#install-on-legacy-android-devices) before switching to UBports' release of Ubuntu Touch.
