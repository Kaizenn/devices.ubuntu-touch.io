---
name: 'VollaPhone'
deviceType: 'phone'
image: 'https://c1.iggcdn.com/indiegogo-media-prod-cld/image/upload/c_limit,w_695/v1584639699/gybpyos99cvoqjyslkwb.jpg'
video: 'https://www.youtube.com/embed/cSRkhPZlT_I'
maturity: .91
tag: 'promoted'

seo:
  description: 'Update your VollaPhone with the latest version of Ubuntu Touch, a privacy focus OS focus on users and teir privacy.'
  keywords: 'VollaPhone, Linux on Mobile, Linux Phone'
---

The [VollaPhone](https://www.volla.online) is an upcoming commercial flagship device with Ubuntu Touch!
