---
name: 'Xiaomi 4'
deviceType: 'phone'
installLink: 'https://gitlab.com/ubports/community-ports/cancro'
maturity: 0
seo:
  description: 'Switch your Xiaomi 4 OS to Ubuntu Touch, as your daily driver, a privacy focus OS.'
  keywords: 'Xiaomi 4, linux for smartphone, Linux on Phone'

externalLinks:
  -
    name: 'Repository'
    link: 'https://gitlab.com/ubports/community-ports/cancro'
    icon: 'github'
---
