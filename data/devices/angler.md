---
name: 'Nexus 6P'
deviceType: 'phone'
portType: 'Halium 7.1'
maturity: .7
externalLinks:
  -
    name: 'Forum Post'
    link: 'https://forums.ubports.com/topic/3678/call-for-testing-google-huawei-nexus-6p-angler-owners'
    icon: 'yumi'

seo:
      description: 'Swith your Nexus 6P smartphone OS to Ubuntu Touch, as your main daily driver operating system.'
      keywords: 'Nexus 6P, linux for smartphone, Linux on Phone'
---
