---
name: 'Sony Xperia X (F5121 & F5122)'
comment: 'community device'
deviceType: 'phone'
portStatus:
  - categoryName: "Actors"
    features:
      - id: "manualBrightness"
        value: "+"
      - id: "notificationLed"
        value: "+"
      - id: "torchlight"
        value: "+"
      - id: "vibration"
        value: "+"
  - categoryName: "Camera"
    features:
      - id: "flashlight"
        value: "-"
      - id: "photo"
        value: "+"
      - id: "video"
        value: "+"
      - id: "switchCamera"
        value: "+"
  - categoryName: "Cellular"
    features:
      - id: "carrierInfo"
        value: "+"
      - id: "dataConnection"
        value: "+"
      - id: "calls"
        value: "+"
      - id: "mms"
        value: "+"
      - id: "pinUnlock"
        value: "+"
      - id: "sms"
        value: "+"
      - id: "audioRoutings"
        value: "+"
      - id: "voiceCall"
        value: "+"
  - categoryName: "Endurance"
    features:
      - id: "batteryLifetimeTest"
        value: "-"
      - id: "noRebootTest"
        value: "-"
  - categoryName: "GPU"
    features:
      - id: "uiBoot"
        value: "+"
      - id: "videoAcceleration"
        value: "-"
  - categoryName: "Misc"
    features:
      - id: "anboxPatches"
        value: "+"
      - id: "apparmorPatches"
        value: "+"
      - id: "batteryPercentage"
        value: "+"
      - id: "offlineCharging"
        value: "+"
      - id: "onlineCharging"
        value: "+"
      - id: "recoveryImage"
        value: "+"
      - id: "factoryReset"
        value: "+"
      - id: "rtcTime"
        value: "+"
      - id: "shutdown"
        value: "+"
      - id: "wirelessExternalMonitor"
        value: "-"
  - categoryName: "Network"
    features:
      - id: "bluetooth"
        value: "+"
      - id: "flightMode"
        value: "+"
      - id: "hotspot"
        value: "+"
      - id: "nfc"
        value: "-"
        bugTracker: "https://github.com/ubports/ubuntu-touch/issues/245"
      - id: "wifi"
        value: "+"
  - categoryName: "Sensors"
    features:
      - id: "autoBrightness"
        value: "+"
      - id: "fingerprint"
        value: "-"
      - id: "gps"
        value: "+"
      - id: "proximity"
        value: "+"
      - id: "rotation"
        value: "+"
      - id: "touchscreen"
        value: "+"
  - categoryName: "Sound"
    features:
      - id: "earphones"
        value: "+"
      - id: "loudspeaker"
        value: "+"
      - id: "microphone"
        value: "+"
      - id: "volumeControl"
        value: "+"
  - categoryName: "USB"
    features:
      - id: "mtp"
        value: "+"
      - id: "adb"
        value: "+"
deviceInfo:
  - id: "cpu"
    value: "Hexa-core 64-bit"
  - id: "chipset"
    value: "Qualcomm MSM8956 Snapdragon 650"
  - id: "gpu"
    value: "Qualcomm Adreno 510"
  - id: "rom"
    value: "32/64GB"
  - id: "ram"
    value: "3GB"
  - id: "android"
    value: "Android 6.0.1"
  - id: "battery"
    value: "2620 mAh"
  - id: "display"
    value: "1080x1920 pixels, 5.5 in"
  - id: "rearCamera"
    value: "23MP"
  - id: "frontCamera"
    value: "13MP"
externalLinks:
  - name: "Telegram - @ubports"
    link: "https://t.me/joinchat/ubports"
    icon: "telegram"
  - name: "Device Subforum"
    link: "https://forums.ubports.com/category/82/sony-xperia-x-f5121-f5122"
    icon: "yumi"
  - name: "Report a bug"
    link: "https://github.com/ubports/ubuntu-touch/issues"
    icon: "github"
---

### Preparatory steps

You can install Ubuntu Touch on the versions F5121 and F5122 of the Sony Xperia X.
Before, you have to take a number of preparatory steps:

1. Ensure you have upgraded the stock firmware at least to Android 8, else you can't flash the OEM binaries for AOSP. It is best to ensure that you have all the latest firmware installed.
2. [Enable developer options](https://www.howtogeek.com/129728/how-to-access-the-developer-options-menu-and-enable-usb-debugging-on-android-4.2/)  for your device,
3. In the developer options, [enable ADB debugging](https://wiki.lineageos.org/adb_fastboot_guide.html) and OEM unlocking,
4. Install ADB on your computer
5. **BEFORE unlocking the boot loader**, [back up your TA partition](https://together.jolla.com/question/168711/xperia-x-backup-ta-partition-before-unlocking-bootloader/), in case you later wish to return to factory state. **You can't do this step at a later stage!**
6. [Get an unlocking code from Sony](https://developer.sony.com/develop/open-devices/get-started/unlock-bootloader/),
7. Reboot to fastboot and unlock the bootloader using the code obtained from Sony
8. Download the [OEM binaries for AOSP from sony](https://developer.sony.com/file/download/software-binaries-for-aosp-nougat-android-7-1-kernel-4-4-loire/), unpack them,
9. Flash them in fastboot (`fastboot flash oem [filename]`)

### Device specifications
|    Component | Details                                                               |
|-------------:|-----------------------------------------------------------------------|
|      Chipset | Qualcomm Snapdragon 650 MSM8956                                       |
|          CPU | 2x 1.8 GHz ARM Cortex-A72, 4x 1.2 GHz ARM Cortex-A53                                          |
| Architecture |                                                                  |
|          GPU | Adreno 510                                                            |
|      Display | 5 in, IPS, 1080 x 1920, 24 Bit                                                              |
|      Storage | 32 GB / 64 GB                                                   |
| Shipped Android Version | 6.0.1 |
|       Memory | 3 GB                                                     |
|      Cameras | 5520 x 4140, 1920 x 1080, 30 fps |
|      Battery | 2620 mAh, Li-Polymer |
|   Dimensions | 69.4 x 142.7 x 7.9 mm |
|       Weight | 153 g |
|      MicroSD | microSD, microSDHC, microSDXC |
| Release Date | June 2016 |

### Maintainer(s)

- [fredldotme](https://forums.ubports.com/user/fredldotme)

### Source repos

#### Kernel

- https://github.com/fredldotme/device-kernel-loire

#### Device

- https://github.com/fredldotme/device-sony-common
- https://github.com/fredldotme/device-sony-loire
- https://github.com/fredldotme/device-sony-suzu

#### Halium

- https://github.com/Halium/halium-devices/blob/halium-7.1/manifests/sony_suzu.xml
